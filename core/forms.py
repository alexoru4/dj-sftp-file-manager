from django import forms
from .models import SFTPConnection


class CustomerForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = SFTPConnection
        fields = ('hostname', 'port', 'login', 'password')

# https://stackoverflow.com/questions/17523263/how-to-create-password-field-in-model-django