from django.db import models
from django.urls import reverse


# Create your models here.
class SFTPConnection(models.Model):
    hostname = models.CharField(max_length=200, null=False)
    login = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    port = models.PositiveIntegerField(default=22, blank=True, null=True)

    def __str__(self):
        return self.hostname

    # # The absolute path to get the url then reverse into 'student_edit' with keyword arguments (kwargs) primary key
    # def get_absolute_url(self):
    #     return reverse('sftpconnection_edit', kwargs={'pk': self.pk})