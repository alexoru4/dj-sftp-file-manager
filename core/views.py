from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, reverse
from django.views.generic import View, ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.core import serializers

from .models import SFTPConnection


class SFTPConnectionList(ListView):
    model = SFTPConnection


class SFTPConnectionDetail(DetailView):
    model = SFTPConnection


class SFTPConnectionCreate(CreateView):
    model = SFTPConnection
    fields = ['hostname', 'login', 'password', 'port']
    success_url = reverse_lazy('core:sftpconnection_list')


class SFTPConnectionUpdate(UpdateView):
    model = SFTPConnection
    fields = ['hostname', 'login', 'password', 'port']
    success_url = reverse_lazy('core:sftpconnection_list')


class SFTPConnectionDelete(DeleteView):
    model = SFTPConnection
    success_url = reverse_lazy('core:sftpconnection_list')


class SFTPConnectionConnect(View):
    def get(self, request, *args, **kwargs):
        request.session['sftp_connection'] = serializers.serialize("json", [SFTPConnection.objects.get(id=kwargs['pk'])])
        return HttpResponseRedirect(reverse("ftp_client:index"))
