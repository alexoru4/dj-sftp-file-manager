from django.urls import path
from ftp_client import views
from django.views.decorators.csrf import csrf_exempt

app_name = 'ftp_client'
urlpatterns = [
    path('', views.index, name='index'),
    path('connect/', views.connect, name='connect'),
    path('tasks/', csrf_exempt(views.tasks), name='tasks')
]
